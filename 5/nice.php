<?php
set_time_limit(0);

/*It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).
It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.
*/

function is_nice($word){
	$vowels = 0;

  for($i=0;$i<strlen($word);$i++){
  	  if(in_array($word[$i], array('a','e','i','u','o'))){
  	  	$vowels++;
  	  }
  }
  if($vowels < 3) return false;


  //double letters
  $letters = array();
  $double = false;
  for($i=0;$i<strlen($word)-1;$i++){
  	if($word[$i] == $word[$i+1]){
  		$double=true;
  		break;
  	}
  }
  if(!$double) return false;


  $denied = array('ab', 'cd', 'pq', 'xy');
  $contains = false;
  for ($i=0; $i < count($denied); $i++) { 
  	 if (strpos($word,$denied[$i]) !== false) {
  	 	$contains = true;
  	 	break;
  	 }
  }

   if($contains) return false;
   return true;
}
$myfile = fopen("input.txt", "r") or die("Unable to open file!");

$count = 0;
while(!feof($myfile)) {
  $word = fgets($myfile);

  if(is_nice($word)){
  	  $count++;
  }
}

fclose($myfile);

die(var_dump($count));