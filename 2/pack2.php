<?php

function get_needed($dimensions){
  $dims = explode("x", $dimensions);
  $dims[0] = (int)$dims[0];
  $dims[1] = (int)$dims[1];
  $dims[2] = (int)$dims[2];
 
  sort($dims);
  $wrap = 2*$dims[0]+2*$dims[1];

  return $wrap + $dims[0]*$dims[1]*$dims[2];
}
$myfile = fopen("dimensions.txt", "r") or die("Unable to open file!");
// Output one line until end-of-file
$total = 0;
while(!feof($myfile)) {
  $dimensions = fgets($myfile);
  if($dimensions=="") continue;


  $needed = get_needed($dimensions);
  $total = $total + $needed;
}
fclose($myfile);

die(var_dump($total));