<?php

function get_needed($dimensions){
  $dims = explode("x", $dimensions);
  $dims[0] = (int)$dims[0];
  $dims[1] = (int)$dims[1];
  $dims[2] = (int)$dims[2];

  $f1 = (int)$dims[0]*(int)$dims[1];
  $f2 = (int)$dims[0]*(int)$dims[2];
  $f3 = (int)$dims[1]*(int)$dims[2];

 
  sort($dims);
  $min = $dims[0]*$dims[1];

  echo "Dimensions: " . $dimensions . "\r\t\rf1: " . $f1 . "\r\tf2: " . $f2 . "\tf3: " . $f3 . "\tmin: " . (int)$min . "\ttotal:" . ((2*$f1 + 2*$f2 + 2*$f3) + $min) . "<br>";
  return (2*$f1 + 2*$f2 + 2*$f3) + (int)$min;
}
$myfile = fopen("dimensions.txt", "r") or die("Unable to open file!");
// Output one line until end-of-file
$total = 0;
while(!feof($myfile)) {
  $dimensions = fgets($myfile);
  if($dimensions=="") continue;


  $needed = get_needed($dimensions);
  $total = $total + $needed;
  var_dump($total);
}
fclose($myfile);

die(var_dump($total));